from random import randint




name = input("You there! Identify yourself. ")


for attempts in range(1,6):

    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Attempt", attempts, ":", name, ", is your date of birth",
        month_number, "/" , year_number, "?")

    response = input("Yes or no? ")

    if response == "yes":
        print("Easy peasy.")
        exit()
    elif attempts < 5:
        print("Absurd! Allow me another.")
    else:
        print("I tire of this. Be gone with you!")
                